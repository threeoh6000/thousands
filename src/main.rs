use rand::Rng;

fn contains_letters(vect: &Vec<String>) -> bool {
	let alphabet = vec!["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
	let mut contains = 0;

	for element in alphabet { 
		if vect.contains(&element.to_string()) { 
			contains = 1; 
		}
	}

	if contains == 0 { return false; } else { return true; }
}

fn gameplay(mut vect: Vec<String>, num: String) -> Vec<String> {
	let mut line = String::new();

	let mut sel = 0;

	println!("Your number is {}, choose where to place it!", num);
	println!("{} | {} | {}", vect[0], vect[1], vect[2]);
	println!("---------");
	println!("{} | {} | {}", vect[3], vect[4], vect[5]);
	println!("---------");
	println!("{} | {} | {}", vect[6], vect[7], vect[8]);

	while sel == 0 {
		let b1 = std::io::stdin().read_line(&mut line).unwrap();
		if line == "a\n" && vect[0] == "a" {
			vect[0] = num.clone();
			sel = 1;
		} else if line == "b\n" && vect[1] == "b" {
			vect[1] = num.clone();
			sel = 1;
		} else if line == "c\n" && vect[2] == "c" {
			vect[2] = num.clone(); 
			sel = 1;
		} else if line == "d\n" && vect[3] == "d" {
			vect[3] = num.clone();
			sel = 1;
		} else if line == "e\n" && vect[4] == "e" {
			vect[4] = num.clone();
			sel = 1;
		} else if line == "f\n" && vect[5] == "f" {
			vect[5] = num.clone();
			sel = 1;
		} else if line == "g\n" && vect[6] == "g" {
			vect[6] = num.clone();
			sel = 1;
		} else if line == "h\n" && vect[7] == "h" {
			vect[7] = num.clone();
			sel = 1;
		} else if line == "i\n" && vect[8] == "i" {
			vect[8] = num.clone();
			sel = 1;
		}
	}

	return vect;

}

fn main() {
	let mut playfield: Vec<String> = vec!["a".to_string(), "b".to_string(), "c".to_string(), "d".to_string(), "e".to_string(), "f".to_string(), "g".to_string(), "h".to_string(), "i".to_string()];
	let mut rng = rand::thread_rng();
	let num = rng.gen_range(1, 6).to_string();

	playfield = gameplay(playfield, num);

	loop {
		if contains_letters(&playfield) == true {
			let num = rng.gen_range(1, 6).to_string();
			playfield = gameplay(playfield, num);
		} else {
			break;
		}
	}
	
	let h1: i32 = playfield[0].parse::<i32>().unwrap() * 100;
	let t1: i32 = playfield[1].parse::<i32>().unwrap() * 10;
	let u1: i32 = playfield[2].parse::<i32>().unwrap();

	let h2: i32 = playfield[3].parse::<i32>().unwrap() * 100;
	let t2: i32 = playfield[4].parse::<i32>().unwrap() * 10;
	let u2: i32 = playfield[5].parse::<i32>().unwrap();

	let h3: i32 = playfield[6].parse::<i32>().unwrap() * 100;
	let t3: i32 = playfield[7].parse::<i32>().unwrap() * 10;
	let u3: i32 = playfield[8].parse::<i32>().unwrap();

	let quiz: i32 = h1 + t1 + u1 + h2 + t2 + u2 + h3 + t3 + u3;
	let john: i32;

	if quiz > 1000 {
		john = quiz - 1000;
	} else if quiz < 1000 {
		john = 1000 - quiz;
	} else {
		john = 0;
	}

	println!("{} | {} | {}", playfield[0], playfield[1], playfield[2]);
	println!("---------");
	println!("{} | {} | {}", playfield[3], playfield[4], playfield[5]);
	println!("---------");
	println!("{} | {} | {}", playfield[6], playfield[7], playfield[8]);
	println!("Your numbers added up to {}!", quiz);
	if john == 0 {
		println!("You won! You reached 1000!");
	} else {
		println!("You were {} off from 1000. Try again later!", john);
	}
}
