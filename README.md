# Thousands
A small maths game

## How to play
A random number is generated and then you select a letter to place your number in.

The goal is to have all the numbers add up to 1000.

The first column (a, d, g) are hundreds

The second column (b, e, h) are tens

The third column (c, f, i) are units
